#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <WinAPI.au3>
#Include <Misc.au3>

#Region
#AutoIt3Wrapper_Icon=G:\programmering\AutoIt3\programs\CPI(CursoPixelInfo)\resources\icon.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Fileversion=1.1
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=y
#AutoIt3Wrapper_Run_Obfuscator=y
#EndRegion

Global $x, $y, $color, $Hpos, $Hcolor, $begin, $colorRec, $savecolorRec, $savecolor
HotKeySet("{Esc}", "exitProgram")
;HotKeySet("^!a", "rec")
HotKeySet("^+c", "saveColor")
Start()

;-------------------------------------------------------------------Start
Func Start()
;~    GUICreate("T", 150, 120,1300,100,$WS_EX_TOPMOST)
   GUICreate("", 200, 120, 1300, 100, Default, $WS_EX_TOPMOST)
   $Hpos = GUICtrlCreateLabel("Mouse position:", 10, 10, 200)
   $x = GUICtrlCreateLabel("0", 20, 30, 200)
   $y = GUICtrlCreateLabel("0", 20, 50, 200)
   
   $Hcolor = GUICtrlCreateLabel("Mouse pixel Color:", 10, 70, 200)
   $color = GUICtrlCreateLabel("#------",20,90,50);
   
   $savecolor = GUICtrlCreateLabel("shift+ctrl+c",100,90,50);
   $savecolorRec = GUICtrlCreateGraphic(150, 90, 20, 15, 300)
   
   $colorRec = GUICtrlCreateGraphic(70, 90, 20, 15, 300)

   $begin = TimerInit()
;~    WinSetOnTop("T", "", 1)
   GUISetState()
  
   Local $dif = TimerDiff($begin)
   ; main loop
   While 1
	  $dif = TimerDiff($begin)
	  if $dif > 80 Then
		 $begin = TimerInit()
		 printMousePos()
		 printPixelColor()
	  EndIf
	  Switch GUIGetMsg()
	  Case $GUI_EVENT_CLOSE
		 Exit

	  EndSwitch
   WEnd
EndFunc   ;==>Start()

;-------------------------------------------------------------------printMousePos
Func printMousePos()
   Local $pos = MouseGetPos()
   
   GUICtrlSetData($x, "X-pos: " & $pos[0])
   GUICtrlSetData($y, "Y-pos: " & $pos[1])
EndFunc   ;==>GetPos

 ;-------------------------------------------------------------------getPixelColor
func printPixelColor()
   Local $pos = MouseGetPos()
   Local $c = PixelGetColor($pos[0], $pos[1])
   
   GUICtrlSetData($color, "#" & Hex($c, 6))
   GUICtrlSetBkColor($colorRec, "0x" & Hex($c, 6))

EndFunc	;==>getPixelColor

 ;-------------------------------------------------------------------saveColor
func saveColor()
   Local $pos = MouseGetPos()
   Local $c = PixelGetColor($pos[0], $pos[1])
   
   GUICtrlSetData($savecolor, "#" & Hex($c, 6))
   GUICtrlSetBkColor($savecolorRec, "0x" & Hex($c, 6))
   ClipPut(Hex($c, 6))
EndFunc
 ;-------------------------------------------------------------------exitProgram
func exitProgram()
   Exit
EndFunc	;==>exitProgram

 ;-------------------------------------------------------------------rec
func rec()
   Local $aMouse_Pos, $hMask, $hMaster_Mask, $iTemp
   Local $UserDLL = DllOpen("user32.dll")

   ; Create transparent GUI with Cross cursor
   $hCross_GUI = GUICreate("Test", @DesktopWidth, @DesktopHeight - 20, 0, 0, $WS_POPUP, $WS_EX_TOPMOST)
   WinSetTrans($hCross_GUI, "", 8)
   GUISetState(@SW_SHOW, $hCross_GUI)
   GUISetCursor(3, 1, $hCross_GUI)

    Global $hRectangle_GUI = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, $WS_POPUP, $WS_EX_TOOLWINDOW + $WS_EX_TOPMOST)
    GUISetBkColor(0xff0000)

   ; Wait until mouse button pressed
   While Not _IsPressed("01", $UserDLL)
	  Sleep(10)
   WEnd
   
   ;Get first mouse position
   $pos = MouseGetPos()
   $x = $pos[0]
   $y = $pos[1]
   Local $hDC, $hPen, $obj_orig
   $hDC = _WinAPI_GetWindowDC(0) ; DC of entire screen (desktop)
   $hPen = _WinAPI_CreatePen($PS_SOLID, 5, "0x90DBFF")
   $obj_orig = _WinAPI_SelectObject($hDC, $hPen)

   
   
   ;_WinAPI_RedrawWindow(_WinAPI_GetDesktopWindow(), 0, 0, $RDW_INVALIDATE + $RDW_ALLCHILDREN)
   
   While _IsPressed("01", $UserDLL)

	  $pos = MouseGetPos()

	  $hMaster_Mask = _WinAPI_CreateRectRgn(0, 0, 0, 0)
	  _WinAPI_DrawLine($hMaster_Mask, 70, 70, 700, 700)
	  $hMask = _WinAPI_CreateRectRgn($x,  $pos[1], $pos[0],  $pos[1] + 1) ; Bottom of rectangle
	  _WinAPI_CombineRgn($hMaster_Mask, $hMask, $hMaster_Mask, 2)
	  _WinAPI_DeleteObject($hMask)
	  $hMask = _WinAPI_CreateRectRgn($x, $y, $x + 1, $pos[1]) ; Left of rectangle
	  _WinAPI_CombineRgn($hMaster_Mask, $hMask, $hMaster_Mask, 2)
	  _WinAPI_DeleteObject($hMask)
	  $hMask = _WinAPI_CreateRectRgn($x + 1, $y + 1, $pos[0], $y) ; Top of rectangle
	  _WinAPI_CombineRgn($hMaster_Mask, $hMask, $hMaster_Mask, 2)
	  _WinAPI_DeleteObject($hMask)
	  $hMask = _WinAPI_CreateRectRgn($pos[0], $y, $pos[0] + 1,  $pos[1]) ; Right of rectangle
	  _WinAPI_CombineRgn($hMaster_Mask, $hMask, $hMaster_Mask, 2)
	  _WinAPI_DeleteObject($hMask)
	  ; Set overall region
	  _WinAPI_SetWindowRgn($hRectangle_GUI, $hMaster_Mask)
	  If WinGetState($hRectangle_GUI) < 15 Then GUISetState()
	  Sleep(100)
	  
   WEnd
   _WinAPI_RedrawWindow(_WinAPI_GetDesktopWindow(), 0, 0, $RDW_INVALIDATE + $RDW_ALLCHILDREN)
   _WinAPI_SelectObject($hDC, $obj_orig)
    _WinAPI_DeleteObject($hPen)
    _WinAPI_ReleaseDC(0, $hDC)
   
EndFunc	;==>rec